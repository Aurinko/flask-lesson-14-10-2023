from flask import Flask, render_template, request
import numpy as np
import tensorflow as tf

app = Flask(__name__)


@app.route('/', methods=["get", "post"])  # http://127.0.0.1:5000 + '/' = http://127.0.0.1:5000/
def predict():
    message = ""
    if request.method == "POST":
        age = request.form.get("age")
        pclass = request.form.get("pclass")
        sex = request.form.get("sex")

        # ['Pclass', 'Sex', 'Age', 'SibSp', 'Parch', 'Fare']
        # model_loaded.predict([[3., 1., 29., 0., 0., 8.]])

        person = [[float(pclass), float(sex), float(age), 0., 0., 8.]]

        model_loaded = tf.keras.models.load_model("titanic_mlp_9_9_5_1")
        pred = model_loaded.predict(person)

        # print(pred)
        message = f"Пассажир выжил с вероятностью {pred[0][0]}"

    return render_template("index.html", message=message)


@app.route('/text/')  # http://127.0.0.1:5000 + '/text/' = http://127.0.0.1:5000/text/
def print_text():
    return "<h1>Some text!</h1>"

app.run()
